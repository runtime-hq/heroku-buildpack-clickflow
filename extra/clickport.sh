#!/usr/bin/env bash

if [ -z "$CLICKPORT_SIGNING_SECRET" ]; then
  echo "CLICKPORT_SIGNING_SECRET environment variable not set. Run: heroku config:add CLICKPORT_SIGNING_SECRET=<your signing secret>"
  DISABLE_CLICKPORT_AGENT=1
fi

if [ -n "$DISABLE_CLICKPORT_AGENT" ]; then
  echo "The Clickport Agent has been disabled. Unset the DISABLE_CLICKPORT_AGENT or set missing environment variables."
fi